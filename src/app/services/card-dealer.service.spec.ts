import { TestBed } from '@angular/core/testing';

import { CardDealerService } from './card-dealer.service';

describe('CardDealerService', () => {
  let service: CardDealerService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(CardDealerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
