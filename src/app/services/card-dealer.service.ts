import { Injectable } from '@angular/core';
import { Card } from '../card';

@Injectable({
  providedIn: 'root',
})
export class CardDealerService {
  private value: number;
  constructor() {}
  cards: any = [
    {
      id: '1A',
      date: '01 Mar 2021',
      title: 'Munich',
      body: 'Munich is the capital and most populous city of Bavaria. With a population of 1,558,395 inhabitants as of 31 July 2020, it is the third-largest city in Germany, after Berlin and Hamburg, and thus the largest which does not constitute its own state, as well as the 11th-largest city in the European Union. The citys metropolitan region is home to 6 million people.',
      country:'Germany',
      backGround: 'orange',
      image:
        'https://image.shutterstock.com/image-vector/munich-germany-skyline-panorama-sky-600w-645548617.jpg',
    },
    {
      id: '1B',
      date: '30 Mar 2021 ',
      title: 'Frankfurt',
      body: 'Frankfurt, officially Frankfurt am Main, is the most populous city in the German state of Hesse. Its 763,380 inhabitants as of 31 December 2019 make it the fifth-most populous city in Germany. On the River Main (a tributary of the Rhine), it forms a continuous conurbation with the neighboring city of Offenbach am Main and its urban area has a population of 2.3 million.',
      country:'Germany',
      backGround: 'lemonchiffon',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-frankfurt-city-600w-1138990625.jpg',
    },
    {
      id: '1C',
      date: '12 Mar 2021',
      title: 'Hamburg',
      body: 'Hamburg is officially the Free and Hanseatic City of Hamburg, is the second-largest city in Germany after Berlin, as well as the overall 7th largest city and largest non-capital city in the European Union with a population of over 1.84 million.',
      country:'Germany',
      image:
        'https://image.shutterstock.com/image-vector/hamburg-skyline-silhouette-flat-design-600w-1729381042.jpg',
      backGround: 'lightblue',
    },
    {
      id: '1D',
      date: '22 Mar 2021',
      title: 'Berlin',
      body: 'Berlin is the capital and largest city of Germany by both area and population. Its 3.8 million inhabitants make it the European Union most populous city, according to population within city limits. One of Germanys sixteen constituent states, Berlin is surrounded by the State of Brandenburg and contiguous with Potsdam, Brandenburgs capital. Berlins urban area, which has a population of around 4.5 million, is the second most populous urban area in Germany after the Ruhr.',
      country:'Germany',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-berlin-city-600w-1133801957.jpg',
      backGround: 'darkslategrey',
    },
    {
      id: '2A',
      date: '24 Mar 2021',
      title: 'New York',
      body: 'New York City (NYC), often simply called New York, is the most populous city in the United States. With an estimated 2020 population of 8,253,213 distributed over about 302.6 square miles (784 km2), New York City is also the most densely populated major city in the United States. Located at the southern tip of the State of New York, the city is the center of the New York metropolitan area, the largest metropolitan area in the world by urban area.',
      country: ' America',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-new-york-600w-1144771568.jpg',
      backGround: 'lavender',
    },
    {
      id: '2B',
      date: '28 Mar 2021',
      title: 'Los Angeles',
      body: 'Los Angeles, officially the City of Los Angeles and often abbreviated as L.A., is the largest city in California. It has an estimated population of nearly 4 million.',
      country: ' America',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-los-angeles-600w-1144771556.jpg',
      backGround: 'rosybrown',
    },
    {
      id: '2C',
      date: '28 Mar 2021',
      title: 'Chicago',
      body: ' Officially the City of Chicago, is the most populous city in the U.S. state of Illinois, and the third most populous city in the United States, following New York and Los Angeles. With an estimated population of 2,693,976 in 2019, it is also the most populous city in the Midwestern United States and the fifth most populous city in North America. ',
      image:
        'https://image.shutterstock.com/image-vector/chicago-city-line-art-vector-600w-678300412.jpg',
        country: ' America',
      backGround: 'red',
    },
    {
      id: '2D',
      date: '28 Mar 2021',
      title: 'Houston',
      body: 'Houston is the most populous city in the U.S. state of Texas, fourth-most populous city in the United States, most populous city in the Southern United States, as well as the sixth-most populous in North America, with an estimated population of 2,320,268 in 2019.',
      country: ' America',
      image:
        'https://image.shutterstock.com/image-vector/houston-tx-usa-skyline-linear-600w-1917299285.jpg',
      backGround: 'magenda',
    },
    {
      id: '3A',
      date: '28 Mar 2021',
      title: 'Mumbai',
      body: 'Mumbai also known as Bombay is the capital city of the Indian state of Maharashtra. According to the United Nations, as of 2018, Mumbai is the second-most populous city in the country after Delhi and the seventh-most populous city in the world with a population of roughly 20 million.',
      country: ' India',
      image:
        'https://image.shutterstock.com/image-vector/mumbai-maharashtra-india-skyline-linear-600w-1960475851.jpg',
      backGround: 'gold',
    },
    {
      id: '3B',
      date: '28 Mar 2021',
      title: 'New Delhi',
      body: 'New Delhi is the capital of India and an administrative district of the National Capital Territory of Delhi. New Delhi is the seat of all three branches of the government of India, hosting the Rashtrapati Bhavan, Parliament House, and the Supreme Court of India.',
      country: ' India',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-new-delhi-600w-1144771562.jpg',
      backGround: 'darkmagenda',
    },
    {
      id: '3C',
      date: '28 Mar 2021',
      title: 'Bangalore',
      body: 'Bangalore  officially known as Bengaluru, is the capital and the largest city of the Indian state of Karnataka. It has a population of more than 8 million and a metropolitan population of around 11 million, making it the third most populous city and fifth most populous urban agglomeration in India.',
      country: ' India',
      image:
        'https://image.shutterstock.com/image-vector/bangalore-india-city-skyline-silhouette-600w-1349052509.jpg',
      backGround: 'darkkhaki',
    },
    {
      id: '3D',
      date: '28 Mar 2021',
      title: 'Hyderabad',
      body: 'Hyderabad is the capital and largest city of the Indian state of Telangana and the de jure capital of Andhra Pradesh. It occupies 650 square kilometres (250 sq mi) on the Deccan Plateau along the banks of the Musi River, in the northern part of South India.',
      country: ' India',
      image:
        'https://image.shutterstock.com/image-vector/hyderabad-skyline-detailed-silhouette-trendy-600w-288752126.jpg',
      backGround: 'darkgray',
    },
    {
      id: '4A',
      date: '28 Mar 2021',
      title: ' Sydney',
      body: 'Sydney is the capital city of the state of New South Wales, and the most populous city in Australia and Oceania. Located on Australias east coast, the metropolis surrounds Port Jackson and extends about 70 km (43.5 mi) on its periphery towards the Blue Mountains to the west, Hawkesbury to the north, the Royal National Park to the south and Macarthur to the south-west',
      country: ' Australia',
      image:
        'https://image.shutterstock.com/image-vector/sydney-city-line-art-vector-600w-648073474.jpg',
      backGround: 'black',
    },
    {
      id: '4B',
      date: '28 Mar 2021',
      title: ' Melbourne',
      body: 'Melbourne is the capital and most-populous city of the Australian state of Victoria, and the second-most populous city in Australia and Oceania. Its name refers to a metropolitan area of 9,993 km2 (3,858 sq mi), comprising an urban agglomeration of 31 local municipalities known as Greater Melbourne',
      country: ' Australia',
      image:
        'https://image.shutterstock.com/image-vector/melbourne-australia-skyline-silhouette-flat-600w-245143036.jpg',
      backGround: 'darkpink',
    },
    {
      id: '4C',
      date: '28 Mar 2021',
      title: ' Brisbane',
      body: 'Brisbane is the capital and most populous city of the Australian state of Queensland, and the third-most populous city in Australia. Brisbane metropolitan area has a population of around 2.6 million, and it lies at the centre of the South East Queensland metropolitan region, which encompasses a population of around 3.8 million.',
      country: ' Australia',
      image:
        'https://image.shutterstock.com/image-vector/brisbane-skyline-silhouette-flat-design-600w-1729381021.jpg',
      backGround: 'darkcyan',
    },
    {
      id: '4D',
      date: '28 Mar 2021',
      title: 'Perth',
      body: 'Perth  is the capital and largest city of the Australian state of Western Australia (WA). It is Australias fourth-most populous city, with a population of 2.1 million living in Greater Perth in 2020. Perth is part of the South West Land Division of Western Australia, with most of the metropolitan area on the Swan Coastal Plain between the Indian Ocean and the Darling Scarp.',
      country: ' Australia',
      image:
        'https://image.shutterstock.com/image-vector/outline-perth-australia-city-skyline-600w-1715558029.jpg',
      backGround: 'cyan',
    },
    {
      id: '5A',
      date: '28 Mar 2021',
      title: 'Bern',
      body: 'Bern  is the de facto capital of Switzerland, referred to by the Swiss as their "federal city" (in German: Bundesstadt, French: ville fédérale, and Italian: città federale). With a population of about 144,000 (as of 2020), Bern is the fifth-most populous city in Switzerland. The Bern agglomeration, which includes 36 municipalities, had a population of 406,900 in 2014. The metropolitan area had a population of 660,000 in 2000.',
      country: ' Switzerland',
      image:
        'https://image.shutterstock.com/image-vector/bern-skyline-monochrome-silhouette-vector-600w-1809311065.jpg',
      backGround: 'darkorange',
    },
    {
      id: '5B',
      date: '28 Mar 2021',
      title: 'Geneva',
      body: 'Geneva is the second-most populous city in Switzerland (after Zürich) and the most populous city of Romandy, the French-speaking part of Switzerland. Situated where the Rhône exits Lake Geneva, it is the capital of the Republic and Canton of Geneva.',
      country: ' Switzerland',
      image:
        'https://image.shutterstock.com/image-vector/geneva-skyline-monochrome-silhouette-vector-600w-1504951208.jpg',
      backGround: 'darkblue',
    },
    {
      id: '5C',
      date: '28 Mar 2021',
      title: 'Zurich',
      body: 'Zurich is the largest city in Switzerland and the capital of the canton of Zürich. It is located in north-central Switzerland, at the northwestern tip of Lake Zürich. As of January 2020, the municipality has 434,335 inhabitants, the urban area (agglomeration) 1.315 million (2009), and the Zürich metropolitan area 1.83 million (2011). Zürich is a hub for railways, roads, and air traffic. Both Zurich Airport and its main railway station are the largest and busiest in the country.',
      country: ' Switzerland',
      image:
        'https://image.shutterstock.com/image-vector/zurich-skyline-monochrome-silhouette-vector-600w-1259110006.jpg',
      backGround: 'darkgoldenrod',
    },
    {
      id: '5D',
      date: '28 Mar 2021',
      title: 'Basel',
      body: 'Basel is a city in northwestern Switzerland on the river Rhine. Basel is Switzerlands third-most-populous city (after Zürich and Geneva) with about 200,000 inhabitants. The official language of Basel is (the Swiss variety of Standard) German, but the main spoken language is the local Basel German dialect.',
      country: ' Switzerland',
      image:
        'https://image.shutterstock.com/image-vector/basel-skyline-monochrome-silhouette-vector-600w-1444284587.jpg',
      backGround: 'red',
    },
    {
      id: '6A',
      date: '28 Mar 2021',
      title: 'London',
      body: 'London  is the capital and largest city of England and the United Kingdom. The city stands on the River Thames in the south-east of England, at the head of its 50-mile (80 km) estuary leading to the North Sea. London has been a major settlement for two millennia, and was originally called Londinium, which was founded by the Romans.',
      country: ' England',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-london-city-600w-1144771577.jpg',
      backGround: 'brown',
    },
    {
      id: '6B',
      date: '28 Mar 2021',
      title: 'Liverpool',
      body: 'Liverpool  is a city and metropolitan borough in Merseyside, England, with a population in 2019 of 498,042, making it the tenth-largest English district by population. Liverpools metropolitan area is the fifth-largest in the United Kingdom, with a population of 2.24 million.',
      country: ' England',
      image:
        'https://image.shutterstock.com/image-vector/liverpool-city-skyline-north-west-600w-1128124232.jpg',
      backGround: 'lightblue',
    },
    {
      id: '6C',
      date: '28 Mar 2021',
      title: 'Oxford',
      body: 'Oxford is a city in England. It is the county town and only city of Oxfordshire. In 2017, its population was estimated at 152,450. It is 56 miles (90 km) northwest of London, 64 miles (103 km) southeast of Birmingham, and 61 miles (98 km) northeast of Bristol.',
      country: ' England',
      image:
        'https://image.shutterstock.com/image-vector/oxford-skyline-monochrome-silhouette-vector-600w-1844497705.jpg',
      backGround: 'lightgreen',
    },
    {
      id: '6D',
      date: '28 Mar 2021',
      title: 'Edinburgh',
      body: 'Edinburgh is the capital city of Scotland and one of its 32 council areas. Historically part of the county of Midlothian (interchangeably Edinburghshire before 1921), it is located in Lothian on the southern shore of the Firth of Forth. Edinburgh is Scotlands second-most populous city and the seventh-most populous city in the United Kingdom.',
      country: ' England',
      image:
        'https://image.shutterstock.com/image-vector/edinburgh-scotland-skyline-silhouette-flat-600w-245143021.jpg',
      backGround: 'pink',
    },
    {
      id: '7A',
      date: '28 Mar 2021',
      title: 'Toronto',
      body: 'Toronto is the capital city of the Canadian province of Ontario. With a recorded population of 2,731,571 in 2016, it is the most populous city in Canada and the fourth most populous city in North America. The city is the anchor of the Golden Horseshoe, an urban agglomeration of 9,245,438 people (as of 2016) surrounding the western end of Lake Ontario, while the Greater Toronto Area (GTA) proper had a 2016 population of 6,417,516.',
      country: ' Canada',
      image:
        'https://image.shutterstock.com/image-vector/toronto-skyline-monochrome-silhouette-vector-600w-695378059.jpg',
      backGround: 'gray',
    },
    {
      id: '7B',
      date: '28 Mar 2021',
      title: 'Montreal',
      body: 'Montreal is the second-most populous city in Canada and most populous city in the Canadian province of Quebec. Founded in 1642 as Ville-Marie, or "City of Mary", it is named after Mount Royal, the triple-peaked hill in the heart of the city.',
      country: ' Canada',
      image:
        'https://image.shutterstock.com/image-vector/montreal-qc-canada-skyline-linear-600w-1943865904.jpg',
      backGround: 'purple',
    },
    {
      id: '7C',
      date: '28 Mar 2021',
      title: 'Calgary',
      body: 'Calgary is a city in the western Canadian province of Alberta. It is situated at the confluence of the Bow River and the Elbow River in the south of the province, in the transitional area between the foothill and the prairie, about 80 km (50 mi) east of the front ranges of the Canadian Rockies, roughly 299 km (186 mi) south of the provincial capital of Edmonton and approximately 240 km (150 mi) north of the Canada–United States border. ',
      country: ' Canada',
      image:
        'https://image.shutterstock.com/image-vector/calgary-ab-canada-skyline-linear-600w-1947523204.jpg',
      backGround: 'white',
    },
    {
      id: '7D',
      date: '28 Mar 2021',
      title: 'Ottawa ',
      body: 'Ottawa  is the capital city of Canada. It stands on the south bank of the Ottawa River in the eastern portion of southern Ontario. Ottawa borders Gatineau, Quebec, and forms the core of the Ottawa–Gatineau census metropolitan area (CMA) and the National Capital Region (NCR).',
      country: ' Canada',
      image:
        'https://image.shutterstock.com/image-vector/ottawa-on-canada-skyline-linear-600w-1946994073.jpg',
      backGround: 'yellow',
    },
    {
      id: '8A',
      date: '28 Mar 2021',
      title: 'Beijing ',
      body: 'Beijing is the capital of the Peoples Republic of China. It is the worlds most populous national capital city, with over 21 million residents within an administrative area of 16,410.5 km2 (6336 sq. mi.). It is located in Northern China, and is governed as a municipality under the direct administration of the State Council with 16 urban, suburban, and rural districts.',
      country: ' China',
      image:
        'https://image.shutterstock.com/image-vector/beijing-city-line-art-vector-600w-765728920.jpg',
      backGround: 'green',
    },
    {
      id: '8B',
      date: '28 Mar 2021',
      title: 'Shanghai',
      body: 'Shanghai is one of the four direct-administered municipalities of the Peoples Republic of China, governed by the State Council. The city is located on the southern estuary of the Yangtze River, with the Huangpu River flowing through it.',
      country: ' China',
      image:
        'https://image.shutterstock.com/image-vector/paper-cut-style-shanghai-city-600w-1192331317.jpg',
      backGround: 'blue',
    },
    {
      id: '8C',
      date: '28 Mar 2021',
      title: 'Shenzhen',
      body: 'Shenzhen  is a major sub-provincial city on the east bank of the Pearl River estuary on the central coast of southern province of Guangdong, China. It forms part of the Pearl River Delta megalopolis, bordering Hong Kong to the south, Huizhou to the northeast, and Dongguan to the northwest, and shares maritime boundaries with Guangzhou, Zhongshan, and Zhuhai to the west and southwest across the estuary.',
      country: ' China',
      image:
        'https://image.shutterstock.com/image-vector/shenzhen-guangdong-province-china-skyline-600w-1980537455.jpg',
      backGround: 'indigo',
    },
    {
      id: '8D',
      date: '28 Mar 2021',
      title: 'Chengdu ',
      body: 'Chengdu is one of the three most-populous cities in Western China, the other two being Chongqing and Xi an. As of 2014, the administrative area housed 14,427,500 inhabitants, the largest in Sichuan, with an urban population of 10,152,632.',
      country: ' China',
      image:
        'https://image.shutterstock.com/image-vector/chengdu-skyline-sichuan-province-china-600w-1264023862.jpg',
      backGround: 'violet',
    },
  ];

  getCard(): any {
    this.value = Math.round(Math.random() * (this.cards.length - 1));
    const selectedCard: Card = this.cards.splice(this.value, 1)[0];
    return selectedCard;
  }
}
