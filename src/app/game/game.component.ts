import { Component, OnInit } from '@angular/core';
import { Card } from '../card';
import { CardDealerService } from '../services/card-dealer.service';
import { Player } from '../models/player';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
  styleUrls: ['./game.component.css'],
})
export class GameComponent implements OnInit {
  public numOfPlayers: number = 3;
  players: Player[] = [];
  constructor(public cardDealer: CardDealerService) {
    const cards: Card[] = this.cardDealer.cards;
    console.log('cards', cards);
  }

  ngOnInit(): void {}

  drawCard(): void {
    const numberOfCards = Math.floor(32 / this.numOfPlayers);
    console.log('number of cards', numberOfCards);
    for (let i = 0; i < 32; ++i) {
      const selectedCard = this.cardDealer.getCard();
      this.players[i % this.numOfPlayers].cards.push(selectedCard);
    }
  }
  assignPlayers(numOfPlayers: number): void {
    this.numOfPlayers = numOfPlayers;
    const availablePlayers: Player[] = [
      { name: 'player1', cards: [], backGround: 'lightblue' },
      { name: 'player2', cards: [], backGround: 'rosybrown' },
      { name: 'player3', cards: [], backGround: 'lightyellow' },
      { name: 'player4', cards: [], backGround: 'lightgreen' },
      { name: 'player5', cards: [], backGround: 'lightcoral' },
      { name: 'player6', cards: [], backGround: 'lemonchiffon' },
    ];
    this.players = availablePlayers.splice(0, this.numOfPlayers);
  }
}
