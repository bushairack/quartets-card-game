import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CardsComponent } from './cards/cards.component';
import { CardDetailsComponent } from './card-details/card-details.component';
import { GameComponent } from './game/game.component';
import { StackComponent } from './stack/stack.component';
import { PlayerComponent } from './player/player.component';
import { CardDealerService } from './services/card-dealer.service';
import { GameMenuComponent } from './game-menu/game-menu.component';
import { TestComponent } from './test/test.component';

@NgModule({
  declarations: [
    AppComponent,
    CardsComponent,
    CardDetailsComponent,
    GameComponent,
    StackComponent,
    PlayerComponent,
    GameMenuComponent,
    TestComponent,
  ],
  imports: [BrowserModule],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
