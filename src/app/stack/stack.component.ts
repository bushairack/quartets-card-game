import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { CardDealerService } from '../services/card-dealer.service';
import { Card } from '../card';

@Component({
  selector: 'app-stack',
  templateUrl: './stack.component.html',
  styleUrls: ['./stack.component.css'],
})
export class StackComponent implements OnInit {
  @Output() cardEvent = new EventEmitter();
  @Input() cardsArrayLength;
  constructor(private cardDealer: CardDealerService) {}
  ngOnInit(): void {}
  getCard(): void {
    this.cardEvent.emit();
    const cards: Card = this.cardDealer.cards.length;
    console.log('card array length', cards);
  }
}
