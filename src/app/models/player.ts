import { Card } from '../card';

export interface Player {
  name: string;
  cards: Card[];
  backGround: string;
}
