import * as url from 'url';

export interface Card {
  title: string;
  id: string;
  country: string;
  date: string;
  body: string;
  backGround: any;
  image: string;
}
