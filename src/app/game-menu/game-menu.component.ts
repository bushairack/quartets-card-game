import { Component, EventEmitter, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-game-menu',
  templateUrl: './game-menu.component.html',
  styleUrls: ['./game-menu.component.css'],
})
export class GameMenuComponent implements OnInit {
  @Output() playerEvent = new EventEmitter();
  ngOnInit(): void {}
  startGame(numOfPlayers): void {
    this.playerEvent.emit(numOfPlayers);
    console.log(numOfPlayers);
  }
}
