## Quartets (card game)
Quartets is a card game with the object to collect 4 cards in a series. Each pack originally contained 32 cards, divided into 8 groups of 4 cards.
### Gameplay
Quartets is played with three or more players, with the aim to win all the quartets (sets of four). Each card usually has a number and letter (1A, 1B, 1C, 1D, 2A, 2B etc. ) in the top left corner of the card.

In this project first we need to choose how many players we have. According to the number of players the cards are equally distributed to each players. The minimum number of players is 3 and maximum is 6.

Sample Output:

![alt text](cardGame.png)